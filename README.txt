
The attached_node module allows users to include a representation of a node within
the body of another node.  The content of a node can thus be embedded within a
Drupal page, or within a blog post, or with a box or DIV or any other HTML
statement.  The format of the representation of the embedded node depends on
stylesheet settings (see the attached_node class in
modules/attached_node/attached_node.css).

A common use of this module is to embed image nodes within other HTML content. If
you have the image.module installed and properly configured this module is able to
display inline images with any node content.



INSTALLATION

1. Unpack the attached_node distribution into the modules directory.

2. Enable the attached_node module in the administer --> modules page 
of the admin section.

3. Once enabled, there will be new option within the "input formats" area of the
"administer" area of Drupal.  To get to it, go "input formats" (admin/filters), and
then click on the first "Configure" link, under "permissions and settings".  This is
the configure link for "filtered HTML". 

Within this page (admin/filters/1), be sure that the box for "Attached Node Filter -
Allows you to create meta tags that link to other nodes" is checked off.  Then click
the "Save configuration" button.

Alternatively, you can pick whichever filter you want (not just "Filtered HTML") to
add attached_node functionality to.


WHAT IT DOES

Once you have enabled the attached node filter as described above, you (or any user
who can post) can add an embedded reference to any page using the syntax:

[node:##]

To do this, the page/node must be using "Filtered HTML" as its input format.  That
can be set as the default, or it can be turned on a node by node basis.



ADVANCED CONFIGURATION/USAGE

If you want your users to be able to configure how the nodes are rendered, you
can click on the "configure filters" tab of the same "Filtered HTML" filter.
Here, in the "Attached Nodes codes" section, you can specify node properties 
that users are allowed to override. A common thing to override is the "title"
property.

There is one exception to this, and that is for image nodes.  There is a parameter
named "res" that users set to specify the resolution of the image to display.
This has to match one of the pre-rendered resolutions configured on the image
admin page.

Click on the "rearrange filters" tab to set the order in which filters are
applied.  Since the Attached Node filter outputs HTML with line breaks and other
things, you'll probably want this filter to be the last one applied to the output.
If, for instance, the HTML filter is run after the Attached Node filter, then
much of the output could be rearranged or removed.  Just give it a large (heavy)
weight.  The default is 10, which should send it to the bottom of the 
list (where it belongs).


ADVANCED USAGE

The tag format is fairly simple.  The most basic tag would be in the following form:

      [node:<node id>]

Parameters follow the <node id> part and are comma separated name="value" pairs:

      [node:123,res="640x480"]
      [node:123,res="original",title="Original version of the picture"]

Note that the values must be encased in double quotes.  This is to allow users
to include commas in the value.  The side effect is that double quotes cannot
be used (currently) without causing problems.

Thanks to:
Rich Cowan for documentation suggestions and contributions.


Questions/comments/etc:
mark@nullcraft.org

Mark Howell
(javanaut)
